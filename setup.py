from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()
    
setup(name='sentientlogger',
      version='0.1',
      description='Sentient logger for ELK',
      url='http://github.com/storborg/funniest',
      author='atkr',
      author_email='atkr@sentient.no',
      license='MIT',
      install_requires=['python-logstash'],
      packages=['sentientlogger'],
      zip_safe=False,
      python_requires='>=3.6',
)