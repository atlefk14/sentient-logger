import logging
import logstash
import sys

class SentientLogger(object):
    def __init__(self, host, port, version, application_name, tcp=False):
        self.host = host
        self.port = port
        self.version = version
        self.tcp = tcp
        self.logger = logging.getLogger(application_name)
        if self.tcp:
            self.logger.addHandler(logstash.TCPLogstashHandler(self.host, self.port, version=self.version)) 
        else:
            self.logger.addHandler(logstash.LogstashHandler(self.host, self.port, version=self.version))
        
        self.logger.setLevel(logging.INFO)
        
if __name__ == "__main__":
    sentient_logger = SentientLogger(host='192.168.10.15', port=5000, version=1, application_name='Das Application', tcp=False)
    test_logger = sentient_logger.logger
    
    test_logger.setLevel(logging.INFO)
    test_logger.error('test logstash error message.')
    test_logger.info('test logstash info message.')
    test_logger.warning('test logstash warning message.')
    test_logger.info('test logstash info message.')
    try:
        1/0
    except Exception as e:
        test_logger.exception('python-logstash-logger: Exception with stack trace!')
